+++
title = "Containers and Docker"
outputs = ["Reveal"]

[reveal_hugo]
theme = "league"
transition= 'concave'
controls= true
progress= true
history= true
center= true


+++

# Containers 

---

## Compute - The State of Play

- What do we remember about compute? 
{{% note %}} Think key words - EC2, VM, ELB, scalable, IaaS etc. Ask about benefits! {{% /note %}}

---

## Compute - The State of Play

- Physical servers are racked in a datacentre they take a long time to provision and are not very flexible
- Amazon AWS, Azure and Google Cloud Platform offer a combination of Physical Servers & Storage and a software programming interface
- Virtual Servers are provisioned in software on top of physical servers, typically you run multiple virtual servers on one physical machine
- Quick to deploy, flexible and highly scalable
- Pay based on consumption rather than in advance 
- e.g. Amazon EC2, Azure, Google Compute Instances
- N.B. We've only really discussed IaaS so far!

---

<img src="https://blogs.bmc.com/wp-content/uploads/2017/09/iaas-paas-saas-comparison.jpg" width="800" height="600" />

---

## What are we trying to solve? 

- Software may not run reliably when ported between different environments 
- e.g Developers laptop to a test environment, staging environment to production or a physical server in a data centre to a VM
- For example if Python 2.7 is running in the data centre while Python 3 is running on your VM there may be weird compatibility issues
- Environmental differences can lead to bugs or securiy issues

--- 

## Virtualization

{{% note %}} Does anybody remember anything about this? {{% /note %}}

---

## Virtualization

- Allows you to use a physical machine’s full capacity by distributing its capabilities among many segregated environments
- Multiple OS's can be running independently on a single machine
- The *hypervisor* is used run guest OS's and share standard resources such as CPU, memory and hard drive that to multiple VMs
- If an application *and* it's environment can be virtualised we can avoid compatibility issues

--- 

## Containers 

- VM's have a large overhead of managing an entire instance of the Operating System e.g. Linux or Windows
- Manual Patching and not very portable between environments
- Containers use the same host operating system so you don't need to install an OS on each instance (or pay for licenses)
- The hypervisor is eliminated - instead virtual containers contain the application and everything that it needs to run (runtime, libraries etc.)
- No virtualised hardware - all containers share the same resources as the host 
- Provide isolation for a instance of the application and it's dependencies
- Much more lightweight than virtual machines - can be started up much faster, many more containers can be hosted and are very easy to share 

---

## Containers

![](https://s16315.pcdn.co/wp-content/uploads/2014/07/docker_vm.jpg)

{{% note %}} Called containers compared to the modern shipping industry as it only works so well due to the sizes of shipping containers being standardised. Before the advent of this standard, shipping anything in bulk was a complicated, laborious process. {{% /note %}}

---

## Docker 

- Containerisation has been around since the 70's with Unix V7 and the early 2000's with BSD Jails and Linux LXC
- Docker is primarily written for Linux where it uses Linux Isolation features to run independent containers within a single Linux instance
- Docker is a tool designed to create, deploy and run applications using containers. Docker popularised the user of containers 
- Docker allows for ease of use, speed, and scalability 
- Originally built for Linux, it can now also run on Windows and macOS

---

## Terminology 

## Further/Useful Reads

